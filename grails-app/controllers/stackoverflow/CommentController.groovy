package stackoverflow

import org.apache.log4j.Logger

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CommentController {
    Logger logger = Logger.getLogger(this.class)

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	def commentService

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def index(Integer max) {
        logger.trace("Begin of index method")

        params.max = Math.min(max ?: 10, 100)
        respond Comment.list(params), model:[commentInstanceCount: Comment.count()]
    }

	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def show(Comment commentInstance) {
        logger.trace("Show " + commentInstance.toString())

        respond commentInstance
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def create() {
        logger.trace("Begin of create comment method")

        respond new Comment(params)
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    @Transactional
    def save(Comment commentInstance) {
        logger.trace("Save " + commentInstance.toString())

        if (commentInstance == null) {
            notFound()
            return
        }

        if (commentInstance.hasErrors()) {
            respond commentInstance.errors, view:'create'
            return
        }

        commentInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'comment.label', default: 'Comment'), commentInstance.id])
                redirect commentInstance
            }
            '*' { respond commentInstance, [status: CREATED] }
        }
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def edit(Comment commentInstance) {
        logger.trace("Edit " + commentInstance.toString())

		def user = SecUser.get(springSecurityService.principal.id)
		if (commentInstance.author.id == user.id){
        	respond commentInstance
		}
		else{
            notFound()
		}
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    @Transactional
    def update(Comment commentInstance) {
        logger.trace("Update " + commentInstance.toString())

        if (commentInstance == null) {
            notFound()
            return
        }

        if (commentInstance.hasErrors()) {
            respond commentInstance.errors, view:'edit'
            return
        }

        commentInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'comment.label', default: 'Comment'), commentInstance.id])
                redirect commentInstance
            }
            '*'{ respond commentInstance, [status: OK] }
        }
    }
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def updateWithoutRedirect (Comment commentInstance) {
        logger.trace("Begin of updateWithoutRedirect method")

		if (commentInstance == null) {
			notFound()
			return
		}

		if (commentInstance.hasErrors()) {
			respond commentInstance.errors, view:'edit'
			return
		}

		commentInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [message(code: 'comment.label', default: 'Comment'), commentInstance.id])
				redirect commentInstance.answer.subject
			}
			'*'{ respond commentInstance, [status: OK] }
		}
	}
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def upVote (Comment commentInstance){
        logger.trace("Begin of upVote comment " + commentInstance.toString())

		println params
		commentService.upVote(commentInstance)
		updateWithoutRedirect(commentInstance);
	}
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def downVote (Comment commentInstance){
        logger.trace("Begin of downVote comment " + commentInstance.toString())

		commentService.downVote(commentInstance)
		updateWithoutRedirect(commentInstance);
	}

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    @Transactional
    def delete(Comment commentInstance) {
        logger.trace("Begin of delete " + commentInstance.toString())

        if (commentInstance == null) {
            notFound()
            return
        }

        commentInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'comment.label', default: 'Comment'), commentInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        logger.trace("Begin of notFound method")

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'comment.label', default: 'Comment'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
