package stackoverflow

import org.apache.log4j.Logger

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SubjectController {

    Logger logger = Logger.getLogger(this.class)
	
	def springSecurityService;

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", addAnswer: "POST"]

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def index(Integer max) {
        logger.trace("Begin of index method")

        params.max = Math.min(max ?: 10, 100)
        respond Subject.list(params), model:[subjectInstanceCount: Subject.count()]
    }
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	def search(String query) {
		logger.trace("Begin of search method")
		
		def subjectInstanceList = Subject.findAllByTitleLike("%${query}%")
		
		
		respond subjectInstanceList, model:[subjectInstanceCount: subjectInstanceList.size]
	}
	
	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def show(Subject subjectInstance) {
        logger.trace("Showing " + subjectInstance.toString())

        respond subjectInstance
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def create() {
        logger.trace("Creating a new Subject")

        SecUser currentLoggedInUser = springSecurityService.getCurrentUser();
		params.put("author", currentLoggedInUser)
		println params
		respond new Subject (params)
    }
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    @Transactional
    def save(Subject subjectInstance) {
        logger.trace("Saving subject " + subjectInstance?.id)

        if (subjectInstance == null) {
            logger.trace("subjectInstance not found")

            notFound()
            return
        }
		
		if (subjectInstance.hasErrors()) {
            respond subjectInstance.errors, view:'create'
            return
        }
        subjectInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'subject.label', default: 'Subject'), subjectInstance.id])
                redirect subjectInstance
            }
            '*' { respond subjectInstance, [status: CREATED] }
        }
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def edit(Subject subjectInstance) {
        logger.trace("Editing " + subjectInstance.toString())

        def user = SecUser.get(springSecurityService.principal.id)

		if (subjectInstance.author.id == user.id){
        	respond subjectInstance
		}
		else{
            logger.trace(user + " not allowed to edit")
            notFound()
		}
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    @Transactional
    def update(Subject subjectInstance) {
        logger.trace("Updating " + subjectInstance.toString())

        if (subjectInstance == null) {
            logger.trace(subjectInstance + " not found")

            notFound()
            return
        }

        if (subjectInstance.hasErrors()) {
            respond subjectInstance.errors, view:'edit'
            return
        }

        subjectInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'subject.label', default: 'Subject'), subjectInstance.id])
                redirect subjectInstance
            }
            '*'{ respond subjectInstance, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    @Transactional
    def delete(Subject subjectInstance) {
        logger.trace("Deleting " + subjectInstance.toString())

        if (subjectInstance == null) {
            logger.trace(subjectInstance.toString() + " not found")

            notFound()
            return
        }

        subjectInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'subject.label', default: 'Subject'), subjectInstance.id])
                redirect controller:"Home", action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'subject.label', default: 'Subject'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def addAnswer () {
		Subject s = Subject.get(params.subject.id);

        logger.trace("[" + springSecurityService.getCurrentUser() + "] addAnswer " + s.toString())

		Answer answer = new Answer(params)
		answer.save(failOnError: true)
		render template:'answersForSubject',model:['answers':s.answers]
	}
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def upVote () {
		Subject s = Subject.get(params.id);

        logger.trace("[" + springSecurityService.getCurrentUser() + "] upVote " + s.toString())

		s.upVote()
		update(s)
	}
	
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def downVote () {
		Subject s = Subject.get(params.id);

        logger.trace("[" + springSecurityService.getCurrentUser() + "] downVote " + s.toString())

		s.downVote()
		update(s)
	}
}
