package stackoverflow

import grails.plugin.springsecurity.annotation.Secured
import org.apache.log4j.Logger

class HomeController {

	Logger logger = Logger.getLogger(this.class)

	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def index() {
		logger.trace("Begin of index method")
		List subjects = Subject.list(max:10)
		subjects.sort{it.lastUpdated}
		
		return [subjects : subjects] // clef : valeur 
	}
}
