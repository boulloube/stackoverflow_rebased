package stackoverflow

import org.apache.log4j.Logger

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AnswerController {
    Logger logger = Logger.getLogger(this.class);

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	def answerService

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def index(Integer max) {
        logger.trace("Begin of index method")

        params.max = Math.min(max ?: 10, 100)
        respond Answer.list(params), model:[answerInstanceCount: Answer.count()]
    }
	
	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def show(Answer answerInstance) {
        logger.trace("Showing " + answerInstance.toString())

        respond answerInstance
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def create() {
        logger.trace("Creating a new Answer")

        respond new Answer(params)
    }	

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    @Transactional
    def save(Answer answerInstance) {
        logger.trace("Saving answer " + answerInstance?.id)

        if (answerInstance == null) {
            notFound()
            return
        }

        if (answerInstance.hasErrors()) {
            respond answerInstance.errors, view:'create'
            return
        }

        answerInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'answer.label', default: 'Answer'), answerInstance.id])
                redirect answerInstance
            }
            '*' { respond answerInstance, [status: CREATED] }
        }
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def edit(Answer answerInstance) {
        logger.trace("Editing " + answerInstance)

		def user = SecUser.get(springSecurityService.principal.id)

		if (answerInstance.author.id == user.id){
        	respond answerInstance
		}
		else{
            logger.debug(user.id + " cannot edit " + answerInstance)
            notFound()
		}
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    @Transactional
    def update(Answer answerInstance) {
        logger.trace("Updating answer " + answerInstance?.id)

        if (answerInstance == null) {
            notFound()
            return
        }

        if (answerInstance.hasErrors()) {
            respond answerInstance.errors, view:'edit'
            return
        }

        answerInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'answer.label', default: 'Answer'), answerInstance.id])
                redirect answerInstance
            }
            '*'{ respond answerInstance, [status: OK] }
        }
    }

    @Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    @Transactional
    def delete(Answer answerInstance) {
        logger.trace("Deleting answer " + answerInstance?.id)

        if (answerInstance == null) {
            notFound()
            return
        }

        answerInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'answer.label', default: 'Answer'), answerInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        logger.trace("answerInstance not found")
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'answer.label', default: 'Answer'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }	
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	def createComment(Answer answerInstance){
        logger.trace("Creating comment on " + answerInstance.toString())

		respond answerInstance
	}
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def updateWithoutRedirect (Answer answerInstance) {
        logger.trace("Updating " + answerInstance.toString() + " without redirect")

		if (answerInstance == null) {
			notFound()
			return
		}

		if (answerInstance.hasErrors()) {
			respond answerInstance.errors, view:'edit'
			return
		}

		answerInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [message(code: 'answer.label', default: 'Answer'), answerInstance.id])
				redirect answerInstance.subject
			}
			'*'{ respond answerInstance, [status: OK] }
		}
	}
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def upVote (Answer answerInstance){
        logger.trace(answerInstance.toString() + " upVote")

		println params
		answerService.upVote(answerInstance)
		updateWithoutRedirect(answerInstance);
	}
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def downVote (Answer answerInstance){
        logger.trace(answerInstance.toString() + " downVote")

        answerService.downVote(answerInstance)
		updateWithoutRedirect(answerInstance);
	}
	
	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
	@Transactional
	def addComment(Answer answerInstance){
		Comment newComment = new Comment(params);
		Answer persistedAnswer = Answer.get(params.answer.id)

        logger.trace("[" + persistedAnswer + "] addComment " + newComment)

        newComment.save(failOnError: true)
		
		redirect action: "show", controller: "subject", id: persistedAnswer.subject.id
	}
}
