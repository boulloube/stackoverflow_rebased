package stackoverflow

import org.apache.log4j.Logger

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SecUserController {

    Logger logger = Logger.getLogger(this.class)
	
	def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def index(Integer max) {
        logger.trace("Begin of index method")

        params.max = Math.min(max ?: 10, 100)
        respond SecUser.list(params), model:[secUserInstanceCount: SecUser.count()]
    }

	@Secured(['IS_AUTHENTICATED_REMEMBERED'])
    def show(SecUser secUserInstance) {
        logger.trace("Begin of show method")

        respond secUserInstance
    }

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def create() {
        logger.trace("Begin of create method")

        respond new SecUser(params)
    }

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    @Transactional
    def save(SecUser secUserInstance) {
        logger.trace("Begin of save method")

        if (secUserInstance == null) {
            notFound()
            return
        }

        if (secUserInstance.hasErrors()) {
            respond secUserInstance.errors, view:'create'
            return
        }

        secUserInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'secUser.label', default: 'SecUser'), secUserInstance.id])
                redirect secUserInstance
            }
            '*' { respond secUserInstance, [status: CREATED] }
        }
    }

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def edit(SecUser secUserInstance) {
        logger.trace("Begin of edit method")

		SecUser currentLoggedInUser = springSecurityService.getCurrentUser();
		if (secUserInstance.id == currentLoggedInUser.id)
        	respond secUserInstance
		else
			render(status: 401, text: g.message(code: "unauthorized"))
    }

    @Transactional
    def update(SecUser secUserInstance) {
        logger.trace("Begin of update method")

        if (secUserInstance == null) {
            notFound()
            return
        }

        if (secUserInstance.hasErrors()) {
            respond secUserInstance.errors, view:'edit'
            return
        }

        secUserInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'secUser.label', default: 'SecUser'), secUserInstance.id])
                redirect secUserInstance
            }
            '*'{ respond secUserInstance, [status: OK] }
        }
    }

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    @Transactional
    def delete(SecUser secUserInstance) {
        logger.trace("Begin of delete method")

        if (secUserInstance == null) {
            notFound()
            return
        }

        secUserInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'secUser.label', default: 'SecUser'), secUserInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        logger.trace("Begin of notFound method")

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'secUser.label', default: 'SecUser'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
