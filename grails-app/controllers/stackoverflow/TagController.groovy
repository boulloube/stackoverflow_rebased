package stackoverflow

import org.apache.log4j.Logger

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TagController {

    Logger logger = Logger.getLogger(this.class)

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def index(Integer max) {
        logger.trace("Begin of index method")

        params.max = Math.min(max ?: 10, 100)
        respond Tag.list(params), model:[tagInstanceCount: Tag.count()]
    }

	@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
    def show(Tag tagInstance) {
        logger.trace("Begin of show method")

        respond tagInstance
    }

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def create() {
        logger.trace("Begin of create method")

        respond new Tag(params)
    }

	
	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    @Transactional
    def save(Tag tagInstance) {
        logger.trace("Begin of save method")

        if (tagInstance == null) {
            notFound()
            return
        }

        if (tagInstance.hasErrors()) {
            respond tagInstance.errors, view:'create'
            return
        }

        tagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tag.label', default: 'Tag'), tagInstance.id])
                redirect tagInstance
            }
            '*' { respond tagInstance, [status: CREATED] }
        }
    }

	
	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    def edit(Tag tagInstance) {
        logger.trace("Begin of edit method")

        respond tagInstance
    }

	
	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    @Transactional
    def update(Tag tagInstance) {
        logger.trace("Begin of update method")

        if (tagInstance == null) {
            notFound()
            return
        }

        if (tagInstance.hasErrors()) {
            respond tagInstance.errors, view:'edit'
            return
        }

        tagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'tag.label', default: 'Tag'), tagInstance.id])
                redirect tagInstance
            }
            '*'{ respond tagInstance, [status: OK] }
        }
    }

	
	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    @Transactional
    def delete(Tag tagInstance) {
        logger.trace("Begin of delete method")

        if (tagInstance == null) {
            notFound()
            return
        }

        tagInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'tag.label', default: 'Tag'), tagInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

	@Secured(['ROLE_ADMIN', 'IS_AUTHENTICATED_FULLY'])
    protected void notFound() {
        logger.trace("Begin of notFound method")

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tag.label', default: 'Tag'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
