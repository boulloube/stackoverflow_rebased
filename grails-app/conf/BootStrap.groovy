import stackoverflow.SecRole
import stackoverflow.SecUser
import stackoverflow.SecUserSecRole

class BootStrap {

	def init = { servletContext ->
	    def roleAdmin = new SecRole(authority: 'ROLE_ADMIN').save()
	    def roleUser = new SecRole(authority: 'ROLE_USER').save()
	    def userAdmin = new SecUser(username: 'admin', password: 'admin', enabled: true)
	    def userUser = new SecUser(username: 'user', password: 'user', enabled: true)
	    userAdmin.save()
		userUser.save()
	    SecUserSecRole.create(userAdmin, roleAdmin)
	    SecUserSecRole.create(userUser, roleUser)
	}
	
    def destroy = {
    }
}
