package stackoverflow

import grails.transaction.Transactional

@Transactional
class CommentService {

    def serviceMethod() {

    }
	
	def upVote (Comment instance){
		instance.vote += 1
	}
	
	def downVote (Comment instance){
		instance.vote -= 1
	}
}
	