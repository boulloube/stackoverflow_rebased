package stackoverflow

import grails.transaction.Transactional

@Transactional
class AnswerService {

    def serviceMethod() {

    }
	
	def upVote (Answer instance){
		instance.vote += 1
	}
	
	def downVote (Answer instance){
		instance.vote -= 1
	}
}
