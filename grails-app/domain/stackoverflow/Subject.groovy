package stackoverflow

import java.util.Date;

class Subject {
	
	String	title
	String	description
	Date dateCreated
	Date lastUpdated
	
	
	static mapping = {
		autoTimestamp true
		answers sort:'vote', order: 'desc'
	}
	int		vote

    static constraints = {
    }
	
	static hasMany = [answers: Answer, tags: Tag]
	
	static belongsTo = [author: SecUser]
	
	void upVote() {
		vote += 1
	}
	
	void downVote(){
		vote -= 1 
	}

}