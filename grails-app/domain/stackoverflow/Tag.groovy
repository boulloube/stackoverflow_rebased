package stackoverflow

class Tag {
	
	String	label

    static constraints = {
    }
	
	static hasMany = [subjects: Subject]
	
	static belongsTo = Subject
}
