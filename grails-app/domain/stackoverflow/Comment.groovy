package stackoverflow

class Comment {
	
   String	content
   Date dateCreated
   Date lastUpdated
   static mapping = {
      autoTimestamp true
	  sort "dateCreated"
   }

    static constraints = {
    }
	
	int vote
	
	static belongsTo = [answer: Answer, author: SecUser]

}
