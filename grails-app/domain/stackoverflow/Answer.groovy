package stackoverflow

class Answer {
	
	String	content
	Date dateCreated
	Date lastUpdated
	static mapping = {
		autoTimestamp true
		sort "vote"
		answerComments sort:"dateCreated"
	}

	int	vote

    static constraints = {
    }
	
	
	static belongsTo = [subject: Subject, author: SecUser]
	
	static hasMany = [answerComments: Comment]

}
