<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'apple-touch-icon-retina.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
    	<r:layoutResources/>
	</head>
	<body>
		<div class="container-fluid">
			<span class="pull-left"><a href="/StackOverflow/home/index"><img src="${resource( dir: 'images', file: 'stackoverflow.png')}"></a></span>
			<g:if test="${applicationContext.springSecurityService.currentUser}">
				<div class="pull-right">
					<g:message code="hello.user"/>
					<g:link id="${applicationContext.springSecurityService.currentUser.id}" controller="secUser" action="show" class="property-value" aria-labelledby="author-label">${applicationContext.springSecurityService.currentUser.username}</g:link>
					,
					<g:link controller='logout'><g:message code="logout" default="Logout"/></g:link>
				</div>
			</g:if>
		</div>
		<g:layoutBody/>
    	<r:layoutResources/>
	</body>
</html>
