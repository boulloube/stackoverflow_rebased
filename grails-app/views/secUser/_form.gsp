<%@ page import="stackoverflow.SecUser" %>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="secUser.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>

	<g:textField name="username" required="" value="${secUserInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="secUser.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>

	<g:textField name="password" required="" value="${secUserInstance?.password}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'accountExpired', 'error')} ">
	<label for="accountExpired">
		<g:message code="secUser.accountExpired.label" default="Account Expired" />
	</label>

	<g:checkBox name="accountExpired" value="${secUserInstance?.accountExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'accountLocked', 'error')} ">
	<label for="accountLocked">
		<g:message code="secUser.accountLocked.label" default="Account Locked" />
	</label>

	<g:checkBox name="accountLocked" value="${secUserInstance?.accountLocked}" />
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="secUser.enabled.label" default="Enabled" />
	</label>

	<g:checkBox name="enabled" value="${secUserInstance?.enabled}" />
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'passwordExpired', 'error')} ">
	<label for="passwordExpired">
		<g:message code="secUser.passwordExpired.label" default="Password Expired" />
	</label>

	<g:checkBox name="passwordExpired" value="${secUserInstance?.passwordExpired}" />
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'userAnswers', 'error')} ">
	<label>
		<g:message code="secUser.userAnswers.label" default="User Answers" />
	</label>
	
	<ul class="one-to-many">
		<g:each in="${secUserInstance?.userAnswers?}" var="u">
			<li><g:link controller="answer" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
		</g:each>

		<li class="add">
			<g:link controller="answer" action="create" params="['secUser.id': secUserInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'answer.label', default: 'Answer')])}</g:link>
		</li>
	</ul>
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'userComments', 'error')} ">
	<label>
		<g:message code="secUser.userComments.label" default="User Comments" />
	</label>
	
	<ul class="one-to-many">
		<g:each in="${secUserInstance?.userComments?}" var="u">
			<li><g:link controller="comment" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
		</g:each>

		<li class="add">
			<g:link controller="comment" action="create" params="['secUser.id': secUserInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'comment.label', default: 'Comment')])}</g:link>
		</li>
	</ul>
</div>

<div class="fieldcontain ${hasErrors(bean: secUserInstance, field: 'userSubjects', 'error')} ">
	<label>
		<g:message code="secUser.userSubjects.label" default="User Subjects" />
	</label>
	
	<ul class="one-to-many">
		<g:each in="${secUserInstance?.userSubjects?}" var="u">
			<li><g:link controller="subject" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
		</g:each>

		<li class="add">
			<g:link controller="subject" action="create" params="['secUser.id': secUserInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'subject.label', default: 'Subject')])}</g:link>
		</li>
	</ul>
</div>

