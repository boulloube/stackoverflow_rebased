
<%@ page import="stackoverflow.SecUser" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'secUser.label', default: 'SecUser')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>

	<body>
		<a href="#show-secUser" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div id="show-secUser" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>

			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>

			<ol class="property-list secUser">
				<g:if test="${secUserInstance?.username}">
					<li class="fieldcontain">
						<span id="username-label" class="property-label"><g:message code="secUser.username.label" default="Username" /></span>
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${secUserInstance}" field="username"/></span>
					</li>
				</g:if>
			
<%--				<g:if test="${secUserInstance?.password}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="password-label" class="property-label"><g:message code="secUser.password.label" default="Password" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${secUserInstance}" field="password"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${secUserInstance?.accountExpired}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="accountExpired-label" class="property-label"><g:message code="secUser.accountExpired.label" default="Account Expired" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="accountExpired-label"><g:formatBoolean boolean="${secUserInstance?.accountExpired}" /></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${secUserInstance?.accountLocked}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="accountLocked-label" class="property-label"><g:message code="secUser.accountLocked.label" default="Account Locked" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="accountLocked-label"><g:formatBoolean boolean="${secUserInstance?.accountLocked}" /></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${secUserInstance?.enabled}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="enabled-label" class="property-label"><g:message code="secUser.enabled.label" default="Enabled" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="enabled-label"><g:formatBoolean boolean="${secUserInstance?.enabled}" /></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${secUserInstance?.passwordExpired}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="passwordExpired-label" class="property-label"><g:message code="secUser.passwordExpired.label" default="Password Expired" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="passwordExpired-label"><g:formatBoolean boolean="${secUserInstance?.passwordExpired}" /></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
			
				<g:if test="${secUserInstance?.userSubjects}">
					<li class="fieldcontain">
						<h2 id="userSubjects-label"><g:message code="secUser.userSubjects.label" default="User Subjects" /></h2>

						<div class="well">
							<ul class="list-unstyled homeSubjectList">
								<g:each in="${secUserInstance.userSubjects}" var="subject">
									<li class="homeSubjectListItem">
										<div class="container">
											<div class="col-md-1 col-lg-1">
												<div class="row">${message(code:'votes.label', default: 'Votes')}</div>
												<div>${subject.vote}</div>
											</div>
				
											<div class="col-md-1 col-lg-1">
												<div class="row">${message(code:'answers.label', default: 'Answers')}</div>
												<div>${subject.getAnswers().size()}	</div>
											</div>
											
											<div class="col-md-10 col-lg-10">
												<g:link controller="subject" action="show" id="${subject.id}"><h4>${subject.title}</h4></g:link>
												${subject.dateCreated}
											</div>
										</div>
									</li>
								</g:each>
							</ul>
						</div>
					</li>
				</g:if>
				
				<g:if test="${secUserInstance?.userAnswers}">
					<li class="fieldcontain">
						<h2 id="userAnswer-label"><g:message code="secUser.userAnswers.label" default="User Answers" /></h2>
						<div class="well">
							<ul class="list-unstyled homeSubjectList">
								<g:each in="${secUserInstance.userAnswers}" var="answer">
									<li class="homeSubjectListItem">
										<div class="container">
											<div class="col-md-1 col-lg-1">
												<div class="row">${message(code:'votes.label', default: 'Votes')}</div>
												<div>${answer.vote}</div>
											</div>

											<div class="col-md-10 col-lg-10">
												<small>On <g:link controller="subject" action="show" id="${answer.subject.id}">${answer.subject.title}</g:link> ${answer.dateCreated}</small>
												<br/>
												${answer.content}
											</div>
										</div>
									</li>
								</g:each>
							</ul>
						</div>					
					</li>
				</g:if>
			
				<g:if test="${secUserInstance?.userComments}">
					<li class="fieldcontain">
						<h2 id="userComments-label"><g:message code="secUser.userComments.label" default="User Comments" /></h2>
						<div class="well">
							<ul class="list-unstyled homeSubjectList">
								<g:each in="${secUserInstance.userComments}" var="comment">
									<li class="homeSubjectListItem">
										<div class="container">
											<div class="col-md-1 col-lg-1">
												<div class="row">${message(code:'votes.label', default: 'Votes')}</div>
												<div>${comment.vote}</div>
											</div>

											<div class="col-md-10 col-lg-10">
												<small>On <g:link controller="subject" action="show" id="${comment.answer.subject.id}">${comment.answer.subject.title}</g:link> ${comment.dateCreated}</small>
												<br/>
												${comment.content}
											</div>
										</div>
									</li>
								</g:each>
							</ul>
						</div>
					</li>
				</g:if>
			</ol>
		</div>
	</body>
</html>
