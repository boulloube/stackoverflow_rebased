<g:if test="${answerComments?.size() > 0}">
	<strong id="comments-label" class="property-label"><g:message code="comments.label" default="Comments" /></strong>

	<ul class="list-unstyled well well-sm">
		<g:each var="comment" in="${answerComments}" >
			<li class="">
				<g:form url="[resource:comment]" controller="comment">
					<span>
						<g:actionSubmit value="-" action="downVote" class="btn btn-danger btn-xs"/>
						<strong>${comment.vote}</strong>
						<g:actionSubmit value="+" action="upVote" class="btn btn-success btn-xs"/>
					</span>
				</g:form>

				<small><g:link id="${comment.author.id}" controller="secUser" action="show">${comment.author.username}</g:link> - ${comment.lastUpdated}</small>

				<p>${comment.content}</p>
			</li>
		</g:each>
	</ul>
</g:if>