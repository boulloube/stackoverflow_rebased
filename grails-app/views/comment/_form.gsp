<%@ page import="stackoverflow.Comment" %>

<div class="fieldcontain ${hasErrors(bean: commentInstance, field: 'content', 'error')} required">
	<label for="content">
		<g:message code="content.label" default="Content" />
		<span class="required-indicator">*</span>
	</label>

	<g:textField name="content" required="" value="${commentInstance?.content}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: commentInstance, field: 'creationDate', 'error')} required">
	<label for="creationDate">
		<g:message code="creationDate.label" default="Creation Date" />
		<span class="required-indicator">*</span>
	</label>

	<g:datePicker name="creationDate" precision="day"  value="${commentInstance?.creationDate}"  />
</div>

