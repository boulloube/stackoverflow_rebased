<%@ page import="stackoverflow.Tag" %>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'label', 'error')} required">
	<label for="label">
		<g:message code="tag.label" default="Label" />
		<span class="required-indicator">*</span>
	</label>

	<g:textField name="label" required="" value="${tagInstance?.label}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'subjects', 'error')} ">
	<label>
		<g:message code="subjects.label" default="Subjects" />
	</label>
</div>

