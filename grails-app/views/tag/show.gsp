
<%@ page import="stackoverflow.Tag" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>

	<body>
		<a href="#show-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>

		<div id="show-tag" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>

			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>

			<ol class="property-list tag">
				<g:if test="${tagInstance?.label}">
					<li class="fieldcontain">
						<span id="label-label" class="property-label"><g:message code="tag.label" default="Tag" /></span>
						<span class="property-value" aria-labelledby="label-label"><g:fieldValue bean="${tagInstance}" field="label"/></span>
					</li>
				</g:if>
			
				<g:if test="${tagInstance?.subjects}">
					<li class="fieldcontain">
						<span id="subjects-label" class="property-label"><g:message code="subjects.label" default="Subjects" /></span>
							<g:each in="${tagInstance.subjects}" var="s">
								<span class="property-value" aria-labelledby="subjects-label">
									<g:link controller="subject" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link>
								</span>
							</g:each>
					</li>
				</g:if>
			</ol>

			<g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${tagInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
