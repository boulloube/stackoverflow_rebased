<%@ page import="stackoverflow.Answer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'answer.label', default: 'Answer')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-answer" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-answer" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>

			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>

			<ol class="property-list answer">
			
<%--				<g:if test="${answerInstance?.author}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="author-label" class="property-label"><g:message code="answer.author.label" default="Author" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="author-label"><g:link controller="user" action="show" id="${answerInstance?.author?.id}">${answerInstance?.author?.encodeAsHTML()}</g:link></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
			
				<g:if test="${answerInstance?.answerComments}">
					<li class="fieldcontain">
						<span id="comments-label" class="property-label"><g:message code="comments.label" default="Comments" /></span>

						<g:each in="${answerInstance.answerComments}" var="c">
							<span class="property-value" aria-labelledby="comments-label"><g:link controller="comment" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					</li>
				</g:if>
			
				<g:if test="${answerInstance?.content}">
					<li class="fieldcontain">
						<span id="content-label" class="property-label"><g:message code="content.label" default="Content" /></span>
						<span class="property-value" aria-labelledby="content-label"><g:fieldValue bean="${answerInstance}" field="content"/></span>
					</li>
				</g:if>
			
				<g:if test="${answerInstance?.dateCreated}">
					<li class="fieldcontain">
						<span id="dateCreated-label" class="property-label"><g:message code="creationDate.label" default="Creation Date" /></span>
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${answerInstance?.dateCreated}" /></span>
					</li>
				</g:if>
			
				<g:if test="${answerInstance?.subject}">
					<li class="fieldcontain">
						<span id="subject-label" class="property-label"><g:message code="subject.label" default="Subject" /></span>
						<span class="property-value" aria-labelledby="subject-label"><g:link controller="subject" action="show" id="${answerInstance?.subject?.id}">${answerInstance?.subject?.encodeAsHTML()}</g:link></span>
					</li>
				</g:if>
			
				<g:if test="${answerInstance?.vote}">
					<li class="fieldcontain">
						<span id="vote-label" class="property-label"><g:message code="vote.label" default="Vote" /></span>
						<span class="property-value" aria-labelledby="vote-label"><g:fieldValue bean="${answerInstance}" field="vote"/></span>
					</li>
				</g:if>
			</ol>

			<g:form url="[resource:answerInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${answerInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>