<%@ page import="stackoverflow.Subject; stackoverflow.Answer" %>

<%--<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'author', 'error')} required">--%>
<%--	<label for="author">--%>
<%--		<g:message code="answer.author.label" default="Author" />--%>
<%--		<span class="required-indicator">*</span>--%>
<%--	</label>--%>
<%--	<g:select id="author" name="author.id" from="${stackoverflow.User.list()}" optionKey="id" required="" value="${answerInstance?.author?.id}" class="many-to-one"/>--%>
<%----%>
<%--</div>--%>

<%--<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'comments', 'error')} ">--%>
<%--	<label for="comments">--%>
<%--		<g:message code="answer.comments.label" default="Comments" />--%>
<%--		--%>
<%--	</label>--%>
<%--	--%>
<%--<ul class="one-to-many">--%>
<%--<g:each in="${answerInstance?.comments?}" var="c">--%>
<%--    <li><g:link controller="comment" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>--%>
<%--</g:each>--%>
<%--<li class="add">--%>
<%--<g:link controller="comment" action="create" params="['answer.id': answerInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'comment.label', default: 'Comment')])}</g:link>--%>
<%--</li>--%>
<%--</ul>--%>
%{--</div>--}%

<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'content', 'error')} required">
	<label for="content">
		<g:message code="content.label" default="Content" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="content" required="" value="${answerInstance?.content}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'subject', 'error')} required" style="display: none;">
	<label for="subject">
		<g:message code="subject.label" default="Subject" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="subject" name="subject.id" from="${Subject.list()}" optionKey="id" required="" value="${answerInstance?.subject?.id}" class="many-to-one"/>

</div>

<%--<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'vote', 'error')} required">--%>
<%--	<label for="vote">--%>
<%--		<g:message code="answer.vote.label" default="Vote" />--%>
<%--		<span class="required-indicator">*</span>--%>
<%--	</label>--%>
<%--	<g:field name="vote" type="number" value="${answerInstance.vote}" required=""/>--%>
<%----%>
<%--</div>--%>