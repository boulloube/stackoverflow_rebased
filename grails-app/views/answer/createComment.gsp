<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<title>${answerInstance.subject.title}</title>
	</head>
	 
	<body>
		<div id="show-subject" class="content scaffold-show" role="main">
			<h1>${answerInstance.subject.title}</h1>

			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>

			<ol class="property-list subject">
				<li class="panel-body">
					<p>${answerInstance.content}</p>
					<div id="comments">
						<g:render template="/comment/commentsForAnswer" model="${['answerComments':answerInstance.answerComments]}"/>
					</div>
				</li>

				<li>
					<g:form name="addComment">
						<g:textArea name="content" placeholder="New comment" rows="5" id="textArea" class="form-control"/>
						<g:hiddenField name="answer.id" value="${answerInstance.id}" />
						<g:hiddenField name="author.id" value="${applicationContext.springSecurityService.currentUser.id}"/>
						<br/>
						<g:actionSubmit class="btn btn-success btn-flat" controller="answer" action="addComment" value="New comment"/>
					</g:form>	
				</li>
			</ol>
		</div>
	</body>
</html>