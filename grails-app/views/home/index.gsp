<html>
    <head>
        <meta name="layout" content="main"/>
        <r:require modules="bootstrap"/>
    </head>
    
	<body>
	    <h1><g:message code="home.index.title" default="Newest subjects"/></h1>
	    
    	<div>
    		<g:link class="btn btn-default" controller="Subject" action="create" params="[]"><g:message code="home.index.newSubject" default="New subject"/></g:link>
    	</div>
    	
		<div class="well">
			<ul class="list-unstyled homeSubjectList">
				<g:each var="subject" in="${subjects}">
					<li class="homeSubjectListItem">
						<div class="container">
							<div class="col-md-1 col-lg-1">
								<div class="row">${message(code:'votes.label', default: 'Votes')}</div>
								<div>${subject.vote}</div>
							</div>

							<div class="col-md-1 col-lg-1">
								<div class="row">${message(code:'answers.label', default: 'Answers')}</div>
								<div>${subject.getAnswers().size()}	</div>
							</div>
							
							<div class="col-md-10 col-lg-10">
								<g:link controller="subject" action="show" id="${subject.id}"><h4>${subject.title}</h4></g:link>
								<g:link id="${subject.author.id}" controller="secUser" action="show">${subject.author.username}</g:link> - ${subject.dateCreated}
							</div>
						</div>
					</li>
				</g:each>
			</ul>
		</div>
	</body>
</html>