<%@ page import="stackoverflow.Subject" %>

<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'subject.label', default: 'Subject')}" />
		<title>${subjectInstance.title}</title>
	</head>

	<body>
		<a href="#show-subject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<%--		<g:form url="[resource:subjectInstance, action:'delete']" method="DELETE">--%>
<%--			<fieldset class="buttons">--%>
<%--				<g:link class="addAnswer" action="addAnswer" resource="${subjectInstance}"><g:message default="Answer"/></g:link>--%>
<%--				<g:link class="edit" action="edit" resource="${subjectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>--%>
<%--				<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--%>
<%--			</fieldset>--%>
<%--		</g:form>--%>

		<div id="show-subject" class="content scaffold-show" role="main">
			<h1>${subjectInstance.title}</h1>

			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>

			<ol class="property-list subject">
				<g:form url="[resource:subjectInstance]" controller="subject">
						<g:actionSubmit value="-" action="downVote" class="btn btn-danger"/>
						<strong style="font-size: x-large;">${subjectInstance.vote}</strong>
						<g:actionSubmit value="+" action="upVote" class="btn btn-success"/>
				</g:form>

				<g:if test="${subjectInstance?.dateCreated}">
					<li class="fieldcontain">
						<span id="dateCreated-label" class="property-label"><g:message code="creationDate.label" default="Creation Date" /></span>
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate format="dd-MM-yyyy" date="${subjectInstance?.dateCreated}" /></span>
					</li>
				</g:if>
				
				<g:if test="${subjectInstance?.author}">
					<li class="fieldcontain">
						<span id="author-label" class="property-label"><g:message code="author.label" default="Author" /></span>
						<g:link id="${subjectInstance.author.id}" controller="secUser" action="show" class="property-value" aria-labelledby="author-label"><g:fieldValue bean="${subjectInstance}" field="author.username"/></g:link>
					</li>
				</g:if>

				<g:if test="${subjectInstance?.description}">
					<li class="fieldcontain">
						<span id="description-label" class="property-label"><g:message code="description.label" default="Description" /></span>
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${subjectInstance}" field="description"/></span>
					</li>
				</g:if>
				
				<div class="">
					<g:if test="${applicationContext.springSecurityService.currentUser}">
						<g:form name="addAnswer">
							<g:textArea name="content" placeholder="${message(code: 'answer.new.label', default: 'New answer')}" rows="5" id="textArea" class="form-control"/>
							<g:hiddenField name="subject.id" value="${subjectInstance.id}" />
							<g:hiddenField name="author.id" value="${applicationContext.springSecurityService.currentUser.id}"/>
							<br/>
							<g:submitToRemote controller="subject" action="addAnswer" update="answers" value="${message(code: 'answer.new.label', default: 'New answer')}" class="btn btn-success btn-flat"/>
						</g:form>
					</g:if>
				</div>
				
				<div id="answers">
					<g:render template="answersForSubject" model="${['answers':subjectInstance.answers]}"/>
				</div>
			</ol>
		</div>
	</body>
</html>