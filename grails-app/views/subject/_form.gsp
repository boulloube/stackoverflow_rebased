<%@ page import="stackoverflow.Subject" %>

<!-- Title display -->
<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>

	<g:textField name="title" required="" maxlength="200" value="${subjectInstance?.title}"/>
</div>

<!-- Description display -->
<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'description', 'error')} required" >
	<label for="description">
		<g:message code="description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>

	<g:textArea rows="5" cols="40" name="description" required=""  maxlength="10000" value="${subjectInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'author', 'error')} required" >
	<g:hiddenField name="author.id" value="${subjectInstance?.author.id}"/>
</div>

<!-- Ansers display -->
<%--<div class="fieldcontain ${hasErrors(bean: subjectInstance, field: 'answers', 'error')} ">--%>
<%--	<label for="answers">--%>
<%--		<g:message code="subject.answers.label" default="Answers" />--%>
<%----%>
<%--	</label>--%>
<%--	<g:select name="answers" from="${stackoverflow.Answer.list()}" multiple="multiple" optionKey="id" size="5" value="${subjectInstance?.answers*.id}" class="many-to-many"/>--%>
<%----%>
<%--</div>--%>