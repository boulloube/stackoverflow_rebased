<g:if test="${answers?.size() > 0}">
	
	<h2 id="answers-label" class="property-label"><g:message code="answers.label" default="Answers" style="margin-bottom: 2px;"/></h2>

	<ul class="list-unstyled">
		<g:each var="answer" in="${answers}" >
			<li>
				<div class="panel panel-default">
					<div class="panel-body">
						<g:form url="[resource:answer]" controller="answer">
							<span>
								<g:actionSubmit value="-" action="downVote" class="btn btn-danger btn-xs"/>
								<strong>${answer.vote}</strong>
								<g:actionSubmit value="+" action="upVote" class="btn btn-success btn-xs"/>
							</span>
						</g:form>
						<small> <g:link id="${answer.author.id}" controller="secUser" action="show">${answer.author.username}</g:link> - ${answer.lastUpdated}</small></br>
						<p>${answer.content}</p>
					
						<div id="comments" style="margin-left: 20px;">
							<g:render template="/comment/commentsForAnswer" model="${['answerComments':answer.answerComments]}"/>
						</div>					
						<g:link controller="answer" action="createComment" id="${answer.id}">Add comment</g:link>
					</div>
				</div>
			</li>
		</g:each>
	</ul>
</g:if>