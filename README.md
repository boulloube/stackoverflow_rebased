# README #
CHANTELOUBE Quentin & BOUENGER Sylvain
ZZ3 F2 Promo16 - ISIMA 

A zip package containing source is available here : https://bitbucket.org/boulloube/stackoverflow_rebased/downloads/StackOverflow_BOULENGER_CHANTELOUBEzip.zip

### How do I get set up? ###

You need to edit grails-app\conf\DataSource.groovy according to your database choice and configuration. 
We used Grails 2.4.4 and Java 1.7 as used in Groovy Grails Tool Suite (GGTS)

All dependencies are referenced in the grails-app\conf\BuildConfig.groovy and will be downloaded on first start.


On start, two user are created :

* an ADMIN one (admin / admin)

* a simple USER (user / user)